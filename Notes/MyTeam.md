# My Team
## IQ - Inbound and Quality

### Useful Links
**(Team Charter)[https://docs.google.com/document/d/1TqxIjV3S7_mpwI1tiE9llqajsQV4jdQazOU2D04Tvyc/edit]**



### What do we do?
Work on a unified platform that controls Inbound and Quality for the warehouse **Inbound** means things coming into the warehouse. For **Quality**, there needs to be an efficient and intuitive receiving process, which inludes audits, corrections and report tooling. 

#### Inbound Visibility
Frequent and early visibility of goods moving into the warehouse is a requirement. The platform should encourage as much visibility into **what and when** goods are moving into the warehouse. Integrations like **ASN**, **Yard Management**, **Dock Management** should be leveraged. 

Focusing on visibility will allow us to unlock higher producitvity for the warehouses, provide shippers with a more effective view on their inbound goods and reveal insights into the inbound process. 

#### Intuitive and High Accuracy
There are different receiving experiences, these need to be unified to decrease code complexity, training, onboarding. This will help us deliver changes and new features faster. 

We want the platform to do most of the heavy lifting for our operators and minimize the decision making for our operators. 

We need to also accurately capture what goods are received, **condition and quantity**. The platform needs to encourage best practices to minimize bad data coming into the Flexe System. 

#### Audits
The platform needs to allow for on demand auditing, cycle counting and reporting. These can be triggered by the shipper or warehouse. The platform needs to improve the efficiency of the auditing process and provide full traceability for stakeholders when changes occur. All of this allows clients and warehouses to run their audits and corrections without the presence of Flexe associates.


### Our Stakeholders
* Shippers
* Warehouses
* AM's and BD's (Account Managers and Business Development/New Customers)
* Deal Flow and Sourcing team
* Ops
* Billing team
* Integrations team


### KPI's
These are the things we measure to track the performance of our platform
* Receiving Cycle Time 
    * How long does it take to process an **appointment**??
* Receiving Accuracy
    * Percentage of corrections made to appointments per line received
* Inventory Accuracy
    * Percentage of cycle counts that find discrepancies per location
* Unused LPN Count
    * Number of LPN's we generate that are not associated to goods
* LPNs putaway
    * Number of LPN's received and put away
* Dock to Stock time
    * Average time taken to receive goods from the trailer and putaway for allocation
    


### How we make decisions
These are the key tenents of decision making for our group and changes to the platform

* Inventory Accuracy - **TOP PRIORITY**
    * Prioritize accuracy over speed
* Simple, intuitive inbound experiences for warehouse associates by automating decisions
    * If a decision can't be automated, add recommendations
    * If recommendations can't be provided, provide data necessary for user to make a wise decision
* Reduce complexity of inbound process
    * Minimize knobs and levers for the user
* Prioritize ease of use and speed of implementation over feature richness


### Team selected goals
* % of fulfillment customers on mobile receving
* Number of inbound configs
* How much ticket count for new reserveations is reduced


### Next 12 months (as of 6/15/2021)
* LPN tracked corrections thru cycle counts
* Multi SKU (aka Rainbow) pallet receving support
* Create cycle counts from mobile app
* Cycle count reporting
* Approve cycle counts via secondary audit
* Scheduling infrastructure for cycle counts
    * Support physical counts
    * Rolling window
    * Individual corrections
* Inbound consolidation
* Migrate inbounds to new service
* Cycle count billing
* Consolidate data entry and correction tools
* Serial capture on inbound
* Serial corrections in cycle counts


### Current risks
* Inbound consolidation is hevily dependent on Ops and customers


### Current features
* Deliveries
* Container Deliveries
* Damages and Returns
* Cycle Counts
* WMS Locations in New Admin
* Mobile Receive App
* Putaway
* Replenishments
