# Inventory Adjustment Fiasco
## What we can do to fix this

* Tool was originally built to fix inbounds for Frito


* Convert modify packinglist and "correct lpn inbound" to inbound adjustments
    * Make sure packinglist audit table is actually used everywhere
* If LPN is decomp'd
    * Allow ops to modify packinglist
    * Warn that CC needs to be created for inventory - give last known location
* What about non-lpn dropoffs?
* Pallet dropoffs?
