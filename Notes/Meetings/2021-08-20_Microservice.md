# Microservice talk for IQ

* Leveraging the IMS service as a template
    * Allows us to snag things like Istio sidecar
    * SRE is already familiar

* jHipster seems rather bulky for our current needs
* Does everyone need their own database?
    * Perhaps have just different schemas 
    * Cost effective
    * Maintenance
    * What about single point of failure?
        * SRE team is actively working on mitigation


* Semi-solidified stack for Kitting
    * Kotlin
    * Ktor
    * DI? (Koin has integration for Ktor, but no current services at Flexe use DI)
    * Postgres - new schema in wh_db
        * Still doing some research for cloud db options
    * Netty - NIO2 uses channels, allows us to use coroutines without memory bloat
    * Istio
    * Prometheus/HoneyComb/Grafana