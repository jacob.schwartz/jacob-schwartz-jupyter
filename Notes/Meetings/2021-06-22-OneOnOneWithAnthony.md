# 1-1 Anthony VP of Eng

* What are you looking for in Senior Engineers?
    * Mentorship
        * Share experiences
        * Walk through PR's 
    * Proactively fix the problems you come across - don't just ignore it
        * Fix the code
        * Or inform others who need to hanlde it
    * Don't assume things are the way they are for any major reason
        * Many smart people had to move fast
        * Point things out and make it better
    * Look beyond the pure engineering - what is the why
* What are some of the big-picture/long-term things I should be keeping an eye on?
    * pull out inbound from the monolith
    * Need more visibiltiy further upstream
        * Trailer
        * Make inbounding more predictable
        * Efficiency
            * Can we speed up inbounding?
            * Many manual processes
            * Messy data coming into inbounding
            * 
* How can I help build team morale for a remote team?
    * Add unstructured time for standups
    * Talk about the weekends, etc. 
* Tennents to keep in mind
    * No such thing as an exception path in the physical world
    * The physical world is messy!!
    * 


* Anthony's role
    * Core prod mgmt, engineering
    * WMS, transit, etc. 
* Looking to integrate allproducts
* Service contracts, created by merging products
    * **SAME DAY AVAILABILITY**
* **SCALING**
    * Business
    * Product
        * 140 engineers by EOY
        * Look for repeatable solutions
        
    * Quality
        * Processes for code changes/features seem to be breaking down
        * 
    * Hiring
        * Everyone is involved
* Operational excellence and quality
    * Focus on quality out of the gate
    * Automated testing
    * **Make sure you have good AC!!!**
    