# Richard's intro to warehouser


Meeting is recorded here:


## Portals
* Admin manages stuff for Flexe (internal client)
* Shipper portal
* Warehouse portal
* Shippers are clients
* Warehouses are clients
* Reservations
    * Links shippers to warehouses
    * Shippers reserve space of the warehouse?
        * Maybe just by capacity
    * Trucks drop it off (outbound side)
    * Eventually shippers ask to move or sell items

## Shipper
* Inbound and outbound
* Inbound
    * Saying what's going into the warehouse
    * Dropoffs (our main side) -- AKA Inbound (formally known as Inbound Shipments)
        * Shipper says what's coming into the warehouse
        * Start point for our customers
        * **Standard Pallets** "Palletized"
            * Whole pallets only
            * Pallet in, pallet out
        * **Container Unload** "Palletization"
            * Can be pallets, eaches, cartons
            * "Unloading a trailer"
            * Process
                1. Select a warehouse - must be active registration
                2. Date to drop
                3. Details
                    * Packaging type: (Floor Loaded, Palletized, Parcel, Unknown)
                        * Floor Loaded - must be converted into pallets
                        * Palletized - already in pallets
                        * Parcel - through mail carrier
                    * Container seal/truck
                    * Purchase Order
                    * Container number, etc
                4. Bill of lading
                5. Inventory
                    * SKUS for goods and their quantity

    * Inventory > Item Master
        * Enter information about goods
        * Eaches, Cartons, Pallets 
            * Dimensions, weight
            * Other shipping restrictions
            * Ti-Hi (pallet aka)
                * Tiers (items per layer)
                * Height (how many layers)
    * Returns
    
    
## Warehouse
**Dropoffs**
* Can confirm shipment requests
* If shipper alters, warehouser will need to re-confirm
* Dropoffs can be completed - accept inventory, etc. 
* Once dropoff is completed, **Putaway** begins

**Inventory > Item Master**
* View information for goods
* Eaches, Cartons, Pallets 
    * Dimensions, weight
    * Other shipping restrictions
    * Ti-Hi (pallet aka)
        * Tiers (items per layer)
        * Height (how many layers)

## Cycle Counts
* Can be done warehouse wide or per location(s)


## Mobile app
* Mostly used for floor
* Starting to get used for inbound

## Models
* Refers to Ruby models
* `ApplicationRecord` refers to database records


## Links
### Environments: <https://wiki.flexe.com/index.php/FLEXE_Environments>
<https://app.flexe.com/admin/dashboard> - PROD
<https://app-sandbox.flexe.com/admin/login> - Sandbox
<https://docs.google.com/drawings/d/15am74an_xGZs9M1KzONe2SAIOC_qMl0MiHT25enKIh0/edit?usp=sharing> - Richard's sketch of our architecture (might have some missing/missinterpreted data)


    
* Outbound
    * Retail is part of this - shipping to retail stores
    * eCommerce - ship to customer
    * Future work: same day shipment
    * Pickups