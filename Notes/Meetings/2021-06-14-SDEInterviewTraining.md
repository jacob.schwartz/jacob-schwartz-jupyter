# SDE Interview Training


* Everyone is nervous, both sides of the table
* Manual: <https://docs.google.com/document/d/1uSW1GfiwlskwxqNyWoi_daLgItQkoj2NIyJq80IMgGE/edit?ts=60c7c3e4#heading=h.3sju2vvcj54t>
* Leveling guidelines: <https://drive.google.com/drive/u/0/folders/1zM5rLfUjCrVPuWQsvihHHZpddqlZcMYk>

* Pre-breifs are required if in interview loop
    * If you can't make it, let the Hireing manager know

* My goal is to make for a positive and engagning candidate experience
* Ask if they need bio breaks
* Keep data gathering factual
* Target is not for candidate to complete the problem, but to see their personality and skills
* If "strong no hire"
    * Have a conversation with recruiter and HR
    * Goal is to feel safe
* Take screenshots
* THANK THEM FOR THEIR TIME!
* Enter notes ASAP
* Don't discuss until debrief
* Debriefs are required
* Interview is a conversation, not a proctored exam

* Be sure to check out level guidelines
* Be prepared
* Don't be afraid to give nudges
* Maybe use copy of prob statement with notes to review

* Be detailed with notes

* Ask questions that peel back the onion

* Don't make a decision during the interview

