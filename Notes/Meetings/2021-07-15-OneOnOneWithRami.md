# 1-1 with Rami Arowesty

* GOLD team - Every process between inbound and outbound?
    * used to own replen and putaway
    * Replenishment
    * Directed Putaway
        * Moving things from the truck to the location in a warehouse
        * Goal to add more direction to put away instead of relying on employees
            * Based on vacancy
            * Based on restrictions
            * Based on lots
            * Based on weight
            * Based on velocity of picking
        * Waiving
            * The process of moving item through warehouse from picking to outbound
            * Think of it as a wave of items moving
            * Picking pulls items
            * Sorting organizes picked items into cartons for efficiency?
        * Replenishments (no real process yet?)
            * Bringing from deep storage to where items can be picked
            * Task based replen?
            * Talk with Jessica Lin about replenishment
            * Lucas is the lead for GOLD
* How can I help our teams work better together?
    * Always feel free to reach out
* What are some points for cross team/subject tesing, etc?
* What are some ways you help build team morale?
    * Coffee break after standup
    * Town-hall bi-weekly
        * Announcements and open qa
    * Monthly team events
    * Sub-team autonomy
        * Avoid isolating people 1 per project

* Rami
    * Worked for Amazon for about 5 years
    * Live events
    * Embedded systems
    
