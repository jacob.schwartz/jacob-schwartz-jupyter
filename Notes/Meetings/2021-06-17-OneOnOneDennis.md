# One on One with Dennis

* Company values
    * Puting customer first
        * Understand our domains
        * Understand the painpoints
        * Understand the flows they use
   * Making it better
       * Opportunity rich
       * A lot of places that can be made better
       * Call it out
   * Jump in
       * Go take care of it
       * Solve the problem
       * Move forward
       * Easier to ask for forgiveness than permission
   * Think Big
   * Work Together
       * Collaborate!
   * Find solutions
       * Don't just identify the problems - solve them

* Frito - this is actually a customer

