# 1-1 with Luke - Principle SRE

* What are some key things we should be on the lookout for in Warehouser?
    * Don't be afraid to jump in and fix old things
    * Advocate for the right way
* Common gotcha's in platform/scaling?
* How is our platform set up?
* What can I do to help?


* Working on a lot of technical debt
* Linting, code coverage
* Automate the toil of engineering - goal
* Lookup 'Site Reliability Engineering' Book and Workbook
* Better deployment pipeline
* Isito (service mesh)
* Better, more defined service ownership
    * Right now everyone owns all the services
    * So nothing gets fixed
* Understand Kubernetes
* Look for new service deployment docs from #service-in-a-day
