# 1-1 with Rebecca Nelson

* Billing team
    * All transactions generate revenue
    * Track all transactions  that happen
    * Calculate and aggregate on monthly basis
    * Eg
        * Get a truckload of pallets of widgets
        * 52 pallets
        * $10.95/pallet
        * Bill rolled up to client
    * Systems
    * BA Team
        * Can create more bespoke tools/queries
        * Driven by config files - merged through git
    * Manual Adjustments
    * Several teams go into managing and negotiating prices for reservations
        * BA's, LA's, Finance, Implementation Project Managers, etc. 
    * Market place project
* How can I help our teams work better together?
    * UoM is a struggle
    * Example: New reservation
        * WH was getting either ASN or PO's 
        * Inbound document was in 'case' (carton) UoM
        * WH received them as eaches instead of cartons
        * Can cause some issues with inventory
        * Rebecca's rule of receiving - "If 'each' is the smallest UoM in a dropoff for a sku, all items should be received in eaches should always be done in eaches"
    * Get rid of pallet equivalencies
    * 
* What are some points for cross team/subject tesing, etc?
* What are some ways you help build team morale