# New Hires meeting with Adam Kapel

* Chief Growth Officer
    * Team that goes to market
    * Branding
    * etc
* Been associated with the company since the beginning
    * Came on full time in Feb 2020
* Came from marketing consulting group
* Background is mostly marketing tech and ad tech
* Flexe is a fast growth company
* Have to know when to tear things down to grow further
* Business Development Associates
    * Introducing Flexe to inew customers
    * Reaching out to potential candidates
* Account Management Group
    * Manages current customers
    * Works to grow current customers
* Sourcing Team
* Marketing Group
    * Grew from 4 people over the last year to several teams
* Rev Ops
    * Set of function that help scale business
    * Salesforce
    * Flexe360
        * Revamping revenue process
    * Billing
* Logistics Strategy
    * Will help to educate the market
* What was the rebranding for?
    * Making the market aware of the many new services we have built over the last few years
    * "We're more than what we were before"
    * Put our flexible services at the very forefront
    * Setting our logo apert from our competetors who use the smae colors and etc as our old logo