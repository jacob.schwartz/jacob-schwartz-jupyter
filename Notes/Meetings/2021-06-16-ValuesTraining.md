# Flexe Values Training

(Presentation Link)[https://docs.google.com/presentation/d/1RLxFWE4Kx4dsgOzUQ0-DsfaDncSp0ZV-fnPGivGGV4Y/edit?usp=sharing]

## Mission
To create the open logistics network that optimizes the global delivery of goods

## Vision
Goods moving seamlessly - when and where they're needed

## Values overview
We want to create the open logistics network that optimizes how goods move around the world.

Values keep us focused on our customers, on working well together

Values were updated in Dec 2019
* Didn't help us reach our mission
* Didn't focus enough on our customers
* Needed clarity

We don't use 'always' or 'never' - revisit decision, tradition for tradition's sake isn't good

Values were created by the employees

### Put customers first
* Clients 
* Internal customers

### Seek solutions
Start with a deep understanding of the problem, then get to solving it

### Work together
Trust each other and have direct conversations. Value diversity, hire great people, build great teams - know we are better together

### Jump in
Our default choice is action. Don't shy away from obsticles - revel in them

### Make it better
From the code we write to the actions we take: leave things better than where you found them

### Think big
We're on a mission. We're building something big and defining the future because we know there's a better way

## How
Take andvantage of the wiki resources

Calibration of values (link in presentation) - What does great look like?

Small Improvement Badges - Recognize your colleagues when you see them living our values

Performance Reviews - 2 times a year

360 feedback - Submit and request feedback out of review cycle

## Livin' it Award
Peer nominated, quarterly

## Challenge
Ask others - esp manager - about the values

Share examples and impression of Flexe culture

Use the wiki

Share awards in small improvements