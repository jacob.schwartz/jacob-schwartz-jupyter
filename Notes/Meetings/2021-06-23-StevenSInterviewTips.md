# Interview tips from Steven Stevenson

* Don't assume they are being hand-wavy
* Try to be objective
* Say things like "I don't have enough details"