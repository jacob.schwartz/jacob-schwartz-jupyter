# Flexe Vision, Mission & Model

* Why build Flexe?
    * Look for a big industry
    * $1.6T - 8% of GDP
    * Logistics
        * Moving stuff from origin to destination
    * Look for industry that is ripe for disruption
    * Companies usually hanlde all their own warehousing
    * E-commerce is surging
        * Requires new type of logistics
        * No more storeshelves
    * Amazon
        * Already disrupted the logistics industry
        * Everyone expects 2 day shipping
    * Technology "platformization" is inevitable
        * Things as a Service is taking over all industries
* Our vision
    * Goods moving seamlessly, when and where needed. 
    * Think like packet routing across the globe
* Our mission
    * To create the **open** logistics **network** that **optimizes** the **global delivery** of **goods**
    * Open: everyone can work with anyone else
    * Network: interconnected system of warehouse "nodes"
    * Optimizes, Global Delivery: Smart, data-driven software that routes goods across the network
    * Goods: Across full supply chain, from raw materials to finished prod
* Our focus
    * Warehouses are static
        * Multi-year leases, large investments, fixed costs
        * Lengthy implementation times
        * Not scalable
    * Businesses are dynamic
        * eCommerce growth
        * Changing customer expectations for delivery speeds
        * Omnichannel mix shifts
* Why is Amazon leading the space so much?
    * Widest selection
    * Lowest prices
    * Fast delivery requires inventory to be close to consumers
* 2 day shipping requires 3 Fulfilment centers
* 1 day shipping requires 12-14
* Same day shipping requires 100+
* To compete, shippers need to take advantage of platforms
* Flexe is the first and largest on-demand network of warehouses
* Goal is tech platform to aggregate everything
    * Massive reach
    * No startup cost
    * One point of access
* The essence of our value proposition is **flexibility**
    * Traditional bespoke solutions are long term and expensive
        * Very little room for changes
    * Our goal is to make shipping look like "the cloud"
    * For a small business, we can be the entire solution
    * For bigger businesses, we can be just part of the solution
        * Seasonal peak fulfilment
        * Same day fulfilment
* This is **HARD**
    * Startup
    * Bootstrapping a 2 sided marketplace
    * Business to Business
    * Created a new category of **On-Demand Warehousing**
    * This is in an old-school industry
* What we have going for us
    * Amazing team
    * Great set of customers, partners, media
    * Have cash to invest
    
**We will create the open logistics network that optimizes the global delivery of goods.**