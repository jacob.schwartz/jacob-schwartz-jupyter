# One on One with Jeka

* Shipping and packaging
* Workflow generation?
* How can I help our teams work better together?
* What are some points for cross team/subject tesing, etc?

* Salmon Accordion team (SA)
* Name from a test name generator
* Owns the outbound process
    * Packing 
    * Shipping
    * Retail
        * Working on retail and eCommerce consolidation
* Also owns reports
    * Eventually moving to a new reports platform
* Owns the packing workflow in the mobile app
    
* Team Ownership: https://flexe-inc.atlassian.net/wiki/spaces/BAT/pages/854065158/Filing+Batdev+Tickets

* Jeka
    * Grew up in Toronto
    * Worked in company that does personal assistant devices
        * Esp. Video Calling over Alexa
    * Been with flexe about 9 months


* Look at documenting/understanding the edges between teams
* Update all the docs!

* What are some ways you help build team morale?
     * After standup - coffee break water cooler chat
     * Cook together over zoom
     * 