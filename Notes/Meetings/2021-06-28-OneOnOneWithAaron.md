# 1-1 with Aaron Webb

* What does IMS handle?
    * Originally WSE
    * IMS's goal is platform
        * Ledger of what things happen and api to execute the move
        * But not any decision making
    * Item Master
    * GraphQL potentially?
    * 
        
* How can I help our teams working well together?
    * Find ambiguity fast and resolve it
    * Look into documentation standards for service, FSDs, etc. 
* What are some ways you help build team morale?
    * Recognize people as people first before all things
    * Clear ownership boundaries
        * Point person should own individual features
        * Allow folks to specialize for individual features

* Most inventory innacurracies happen in inbound
    * That's why IQ owns quality AND inbound

* Putaway was part of GOLD (picking to packing - golden pipeline) originally
    * Pulled to inbound because the putaway algorithm is easy and actually separate
    * Will eventually be part of inventory placement
        * Will also include replenishment
        * Difference of pulling items from truck or elsewhere in the warehouse
    * 

* Salmon Accordion
    * Name was from some test generator
    * Packing and shipments
        * Eventually shipment workflow management
    * Split into xport as well