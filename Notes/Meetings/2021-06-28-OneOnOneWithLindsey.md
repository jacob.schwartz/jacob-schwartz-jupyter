# 1-1 with Lindsey Peters
* Product Manager of Inbound

* What are some of the big picture things I should keep in mind for inbound?
    * We're at a pivot point where we need to slow down and deliberately plan for scaling
    * Focus on polishing product. 
* What are some of the painpoints we have in inbound?
    *
* Which is the place that needs the biggest dev work? Putaway/Replenishment?
    * 

* Retail vs eCommerce
    * Retail assumes full pallets
    * eCommerce assumes eaches
    * These are hardcoded rules/assumptions that are beginning to break down
* Shipper and Warehouse are not in direct contract with eachother
    * Contracs are direct to us
    * Warehouse somtimes hides some info
* Worked in supply chain for a long time
