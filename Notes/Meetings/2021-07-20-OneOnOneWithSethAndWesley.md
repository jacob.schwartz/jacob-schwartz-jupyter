# 1-1 with Seth Johnson and Wesly McNeill

* Transport team
    * Outbound transportation
    * Core flow fr parcel transport
        * Generate valid shipping label
        * Attach to package
        * Carrier
        * We can do rate shopping
        * Getting into owning and managing carrier relations for smaller customers
        * Common problem, lack of carrier capacity
    * LTL
        * Freight
    * FTL - Full truck load
    * goal to expand more rate planning
    * Might be involved with our kitting project
* How can I help our teams work better together?
* What are some points for cross team/subject tesing, etc?
    * Code quality plugins for gitlab
    * Project documentation guidelines
* What are some ways you help build team morale
    * Take first step to know the team
    * Video games
    * Take a focus on celebrating teams
    * Find what should be celebrated that isn't currently being celebrated
    * Virtual escape room
    * https://about.gitlab.com/company/culture/all-remote/
    