# Mobile management info from Tyler


* Recording: <https://flexe.zoom.us/rec/share/17Kpl2ycjOf4i-Kk3NSplfC7sNKTdthf7wX5fKc0_IFtYYabEAuL_jmk9K3qZERt.epWH2S_cfzhwmEkE> Passcode: 0m8iNkc$ 

* To add a device, you need to create a mobile user from the warehouse manager
* Two different mobile devices supported
    * One has a physical keyboard, the other doesn't
    * Specs are in the readme of the repo
* Config files manage things like server locations
* wf - contains all workflows
* repo: `flexewh-mobile`

## Receiving
* Only confirmed dropoffs will be shown in the mobile platform
* Locations are tied together by scanning the location
    * For receiving, it must be an "inbound staging localtion"
* Depending on the "expect LPN flag" the workflow will adjust
* For scanning, there's a way to enable a textbox to simulate scanning the LPN
* App collects metadata
    * Lot code - MFG code, refers to a particular batch of items
    * Expiration date
    * Must be enabled for that sku
* Once receiving is complete from the mobile side, it will enter the data into the dropoff record - access by the web console
* Receive app was the first app Flexe did
    * Uses a bit of an older design flow
    * Old activity flow
* ASN workflow
    * LPN's are verified



## Cycle Count
* Request to count skus - reconsile expected vs actual inventory
* Uses "fragment controlled activities"
* VM's can be shared between fragments
* Requires the separate Scala service for cycle count
    * SBT Scala Build Tool
    * `getty start` and `getty stop`
    * Separate docker container
    * ruby gem is built from a swagger client - consumed by warehouser
* Once a cycle count is started, the location is locked down - no adding/removing until done
* Once started from web app, it is enabled in the mobile app
* SKU workflow is counting items
* LPN workflow is confirming location, etc. 
    * LPN's are typically wrapped pallets
    * When items/cartons pulled from LPN, the LPN is destroyed and the pallet becomes loose skus
* Cycle counts need to be completed/finalized by web application before it's actually finished - manager reviews
