# Meeting 1-1 w Dennis

## Specific onboarding
* Focus on the 1st 90 onboarding

## Focus on problem space - choose a focus
* Receiving/inbound
    * Richard
* Putaway
    * Me?
* Replenishments
    * Me?
* Quality
    * Probably new person in July

## Dennis' Management Style
* Not a micromanager
* Hold ourselves accountable to set deadlines


## On call
* Probably after 90 days

## 1-1 meetings
* 2 times a week


## Todo's
* Ask Richard about Ruby, etc. 
* Ask questions!
* Check in about onboarding, provide suggestions
* Lean on others
