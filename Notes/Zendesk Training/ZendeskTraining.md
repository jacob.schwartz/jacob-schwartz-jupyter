



# New Admin Intro
* New admin platform will eventually replace the legacy one
* Ops uses this to handle adjustments to things like LPNs, reservations, etc
* WMS manages inventory adjustments
* Finance does SKU equivalents reports
* Reservations handles new reservations
* DLQ handles failed messages between services
* Warehouses set batching methods

## WMS locations
* Used primarily to modify shipping orders with incorrect data

## Reservations
* Add reservations and manage scopes
    * Scopes are the agreements between the warehouse and shipper
        * Billing
        * Allowed goods
        * Inventory groups - pricing can be added by group
        * Enable services
            * Storage. container unload, pallets, eCommerce Fulfillmnent, Retail Fulfillment, Returns, Damaged Inventory
        * Configs for services
        



## Batching
From the wiki:
> This feature allows warehouses to have shipments available to them at pre-determined intervals or as soon as a customer creates an order 
    
* Automatic Batching
    * When an order flows through Flexe from shipper to warehouse platforms and batches automatically even if there is only one order
* Manual Batching
    * New orders must be waved manually by ops

## Finance/Watermark

## Dead Letter Query
* Used for developers only
* Holds messages that were passwed between the microservices that couldn't be delivered or processed


# Old Admin Intro
* New admin page can be accessed via the Dashboard dropdown

## Configurations Dropdown
* Companies
    * Page shows all shippers and warehouses Flexe partners with
* Reservations
    * All active, completed and new reservations are logged here
* Reservations (Fulfillment)
    * Each reservation has its own specific fulfillment configs
    * View configs and edit eCommerce, Retail and Overflow strategies
* Reservations (Locations)
    * View and edit reservation to enable/disable warehouse or use auto-move/locations system
* Reservations (Scopes)
    * Scopes are the agreements between the warehouse and shipper
* Reservations (Walmart)
    * Reservations for Walmart
* Warehouses
    * Shows all warehouses flexe partners with
* Warehouses (Locations)
    * Warehouse locations get specific defaults, edit here
* Warehouses (SSCC Distributor)
    * Not used
* Developers
    * Delayed Jobs - issue/error logs
    * Destination URI - Webhook destination settings

## Finance and Legal
* Adjustments
    * Add any billing adjustments
* Warehouse Labor Charges
    * Enter VAS hourly charges
* Agreements
    * User agreement and addendums
* Agreements - WSA
    * Reservation agreements 

## Inbound
* "P"allet Deliveries
    * View/edit specific palletiezed dropoff
* Container Deliveries
    * View and edit information for a container

## Inventory Levels, Moves, Tracking
* Inventory Adjustment
    * Log of cycle counts
* Inventory Tracking Data
    * Log of all item codes in Flexe
* Location contents
    * Log of all SKUs with their quantities stored at each location per reservations
* Location Movement Log
    * All movements done through mobile devices
* Locations
    * Log of all locations created in Flexe
* Loose goods location arrivals
    * Items are tracked based on when they first arrived to the warehouse
    * Used to determine when items need to leave the warehouse
* LPN Snapshots
    * Log of all snapshots of each LPN received
* MHE (rebin carts)
    * Utilized by dev team for rebin/sorting operations
    * Walmart and Sam's Club?
* Mobile Moves & Picks
    * Log of all movements using the movile device (Same as Location Movement log??)
* Pallets
    * Log of all pallets 

## Item Master
* View and disable any SKU
* SKU entries can be modified directly here

## Outbounds
* eCommerce Fulfillment Shipments
* Retail Fulfillment Orders

## Reporting
* Create reports
    * Shipments
    * Inventory On Hand
    * Fulfilment
    * more
    
## User Management
* Manage all users here
* FLEXE Admins
* Mobile Users
* Users

## Zendesk
* Used for customer ticket management



# Shipper Page
* Shippers only see this page
* White background on the sidebar means shipper 
* Name displayed is the primary admin (when logging in via the su operation from admin)

## Users
* Admins can add new shipper users
* Users can be deactivated, but not removed
* Roles
    * Manager
    * Billing
    * Operations
    * Billing
* Different roles can be assigned based on reservation

## File Exchange
* Record of all CSV uploads

## Dropoffs
* Shippers create dropoffs (plans to ship items)
* Dropoffs must be scheduled 48 hours in advance

## Returns
* Used mostly by warehouse side

## eCommerce Panels
* eCommerce Orders - requests from customer
* eCommerce Shipments - Fulfilments of order

## Item Master
* Create/edit items (skus)
    * Enter dimensions, weights
    * Information for cartons and pallets, including dimensions

## Groups
* Allows shippers to view specific information for a group/type of shipment at a specific reservation

## Cycle Counts
* Shippers can request a cycle count for a particular SKU


## Inventory on Hand Report
* Generates a PDF report to user's email



# Warehouse Page
* Warehouse users only see this page
* Black background on sidebar
* Name display is primary admin
* Can create mobile device users and delete them

## Dropoffs
* Warehouser can confirm items
    * Enter shippable items and damaged items
    * Staging location
    * Etc

## Returns
* File any returns received on behalf of shipper

## Pickups


## Retail
* Enter data for shipping to retail stores (pallets)

## eComm Waving
* Group some shipments into batches
* Can wave by templates, carriers, reservations

## eComm Batches
* Handle puting items together for shipping outside of warehouse
* Just things being processed together
* Might be things shipped to multiple receivers


## Item Master
* Primarily for reference, no edits allowed

## Cycle Counts
* Lists all cycle counts for that warehouse
* Can create cycle counts
    * Can count by location or by item


## Setup
* Manage reservations

## Locations
* View history of actions that happen within a location of the warehouse

## Tasks
* Listing of all tasks that are happening or have happened on the warehouse floor
* Picking, moving, etc. 
