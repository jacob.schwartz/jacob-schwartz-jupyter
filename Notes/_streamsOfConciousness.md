# Anything I don't have a home for yet




* rake jobs:workoff
    * Seems to be some sort of way to complete the shipper label?
    
    
## Istio notes

* More than a reverse proxy
* Infinite reverse proxies and they're all different
	* Istio sidecar
* OLD VERSION
	* Used to do auth, load balancing etc
	* Late Aug 2021 - version changed
* NOW
	*`istiod` - istio daemon
	* Auto injection, handles sidecar integrations
* Sidecares are **Envoy Proxies**
* Istio Gateways
	* CURRENTLY NEEDS CLEANED
	* Creates an ingress in kubernetes
		* Service points to pods (deployments)
	* 3 Gateways (our setup, still needs to be fixing)
		* External
		* Internal
			* Authentication conflaited with authorization


* Authorization for WO
	* Require information in api contract?
	* How can we centralize 

* SSH into the pod in the cluster and make the request from curl

* prestop - what is this? -- quiet period for netty/ktor




## Query
labels."k8s-pod/app"="warehouser-http"
jsonPayload.named_tags.request_path=~"\/api\/v2\/containers\/.*"


## Decompose - high level steps (maybe in order)

0. Build edges for other groups to call into service
1. Remove all deprecated calls and switch to edges
2. 
3. Create container


## Removing Overflow?
* Gap analysis for customers needing custom attributes
    * Wifi enabled warehouses
    * Non-wifi enabled warehouses can use locations still
* Can we add LPNs to CSV uploads?
* Allow warehouses to enter lpns via the web form
    * https://flexe-inc.atlassian.net/wiki/spaces/TA/pages/1593606579/2021+04+06+Tour+of+Transformco+in+Ocala+FL
* Needed features before transitions can happen
    * Pick by LPN
    * Custom attributes on LPN
    * Recall by LPN
        * By pick recommendation?
    * LPN in webapp (ASN? Definitely at the warehouse level though)
* teams of work
    * IQ
    * MIST
    * PAS
    * IMS