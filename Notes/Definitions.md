# Definitions and Etc.

## Terms
Take a look here too: <https://flexe-inc.atlassian.net/l/c/ZfupNKBk>

### Picking

### Recieving

### Replenishing

### Putaway

### Quality

### IQ Inbound and Quality
My team. Deals with any goods coming into the warehouse. 

### LPN License Plate Number
Number assigned to a pallet. Used in tracking entire pallet. Barcode

### ASN Advanced Shipping Notice
Set of tech that lets warehouses know when they will get a shipment and what should be there. 

### Yard Management

### Dock Management

### Appointment

### AM Account Manager
Person/group who manages a current customer

### BD Buisiness Development
Person/group who looks for new customers

### Cross Dock
products go from one transportation vehicle to another without being stored in between

### SRE

### LTL Less Than Truckload
Trucks that aren't fully loaded??

### DPA - Directed PutAway

### WSA - Warehouse Service Agreements
Agreements based on our warehouse reservations

### Reservation
Unique agreements between shippers and warehouses

### LA - Logistics Analyst
Flexe employee, handles resservations

### COE - Correction of Error
Think of as root cause analysis 

## Applications

### Move That
Applcation that handles moving items in warehouse, including from trucks

### Cycle count (aka Inventory Adjustment)
Process to reconcile allocated vs. actual. Can be requested by customer and at warehouse. 