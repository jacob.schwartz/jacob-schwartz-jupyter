# Sys Design questions

## DB
* Why SQL/No SQL?

## Data
* How are you going to scrub data?
* Partial success for uploads?

## Resiliancy
* How would you track the success/errors in your service?
* How would you monitor the success for your product?