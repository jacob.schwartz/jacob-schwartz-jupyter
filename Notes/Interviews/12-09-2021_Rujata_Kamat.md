# Rujata Kamat
## Summary
Seems a bit nervous.

## Checklist
*  Able to come up with optimal, simple algorithm. 

*  Code is efficient and performant

*  Able to come up with an extensive list of test cases. 

*  Able to proactively ask questions to clarify problem ambiguity. 

*  Able to produce clean, extensible code that implements the algorithm. 

*  Respond to interviewer questions and feedback, adjust their approach based on questions/feedback

*  Able to make trade-offs and justify decision making approach

*  Able to explain runtime complexity of the code produced

*  Code is easy to read and easy to maintain (modularized; with appropriate level of abstractions)

*  Code handles most edge cases.


## Raw Notes:


### Intro
* 

### Implementation
* Starting with line by line
* Bit of a slow start
* Debating between char array and builtins
* Moving towards token approach
* Took a while to get to double booleans for comment type
* Had to be pushed for string literals
* Thought about a better design perhaps
* Didn't hit escaped quote
* 