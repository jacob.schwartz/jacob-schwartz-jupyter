# Eric Barke - Sr SDE
## Java comment parsing
## Summary
Candidate frustrated about communication on the missing zoom for previous interview. Was told he didn't show, instead of asking if there was an issue. 

I'm on the fence with Eric. I think he is a strong coder and was able to start breaking the problem down well. Our interaction was pleasant and he was able to answer questions and take guidance pretty well. I may have given him some more help than I should have to get him pointed into some of the edge cases (string literals, etc) but he was able to get a pretty solid working algorithm in the end.

Caveat: he did not finish the mutliline approach. We ran out of time. 

So, for a senior engineer, I'm stuck in the middle. I'm interested to see his work in the other coding interview. 

## Checklist
*  Able to come up with optimal, simple algorithm. 
    * Mostly, but didn't finish multiline quotes
*  Code is efficient and performant
    * Yes
*  Able to come up with an extensive list of test cases. 
    * Was hoping for more
*  Able to proactively ask questions to clarify problem ambiguity. 
    * Wish he would have more
*  Able to produce clean, extensible code that implements the algorithm. 
    * Yes
*  Respond to interviewer questions and feedback, adjust their approach based on questions/feedback
    * Yes
*  Able to make trade-offs and justify decision making approach
    * Somewhat
*  Able to explain runtime complexity of the code produced
    * Missed asking
*  Code is easy to read and easy to maintain (modularized; with appropriate level of abstractions)
    * Somewhat
*  Code handles most edge cases.
    * Yes - except multiline comments

## Gdoc
https://drive.google.com/open?id=1wV2hk8O-CWwDd1lHUaNYdlD6JscIMf1Bd6ltSKaneUM&authuser=0

## Raw Notes:
* Writing in C#
* Starting with some scaffolding
* Looking at LINQ to help solve - I don't think that would be a great option
* Moving on - working with indexOf/remove functions - hasn't hit on string litterals and other edgecases yet
* I'll ask for them a bit later
* Might be flustered - a number of mistakes in expected method returns I wouldn't expect from a Sr who know's their language
* How would you test RemoveDoubleSlashComments?
    * comments at start
    * comments at end
    * comments at start of file
    * comments at end of file
* I reminded him about string literals before starting to get to c++ style comments
* Interesting... using `Stack<string>`, let's see where he goes with it.
* Almost hitting on token tracking
* Ok, I think he's shifting to token tracking using the stack. 
* Allowing to assume source code with unix-style line endings
* A little sloppy with cases here...
    * Missing check for type of comment we're in, not cleaning up stack appropriately
* Ok, starting to notice some holes in logic with quotes
* Don't think we're going to have time to do multiline comments
* Asked about it, but Eric went back to the old indexOf method? Why not build it into the stack approach?
    * Yeah, that approach won't work.... doesn't take care of string literals. Not sure why we went back here. 

* Very scared to work on a monolith and get stuck in maintenance
    * Doesn't really want to leave c#, but not opposed