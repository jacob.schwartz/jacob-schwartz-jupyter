# Tristan Mills - Sr SDE
## Java comment parsing

## Summary
Depending on the results of his long coding, I'm inclined to pass on Tristan. I think nerves played a lot in this interview though. From a communication perspective, I think he did quite well, but there were some misses in the actual implementation that I'm concerned about. 

However, TDD is a good thing to see even in a problem like this. Towards the end, Tristand did begin see how he could adjust his algorithm to better deal with parsing mid and multiline blocks. 


## Checklist
*  Able to come up with optimal, simple algorithm. 
    * Almost, hampered by line-by-line approach
*  Code is efficient and performant
    * Yes
*  Able to come up with an extensive list of test cases. 
    * Was able to articulate several more than the original TDD
*  Able to proactively ask questions to clarify problem ambiguity. 
    * Would like to have seen more
*  Able to produce clean, extensible code that implements the algorithm. 
    * Things started to get a bit messy, I think due to the line-by-line approach
*  Respond to interviewer questions and feedback, adjust their approach based on questions/feedback
    * Mostly, I tried not to give too much help without being prompted
    * When nudged about a few things like tracking quotes, etc. Tristan pivoted well
*  Able to make trade-offs and justify decision making approach
    * Missing some checking on alternatives. Seemed to get stuck in one approach
*  Able to explain runtime complexity of the code produced
    * Didn't get to this
*  Code is easy to read and easy to maintain (modularized; with appropriate level of abstractions)
    * Middling
*  Code handles most edge cases.
    * Started to handle, didn't get everything

## Gdoc
https://docs.google.com/document/d/16KVI-czUseqcVUJPSyeeWO_78ZZlYee9-AmbJIA-jzg/edit

## Raw Notes:
* Seems a tad nervous
* Trying to use some TDD - I reiterated that he doesn't need to worry about having tests unless he wants
* Interesting... using Scanner over string input
* Ah, going for line by line first.
* Would like to have seen Tristan start with listing out the cases 
    * Process us write one test, write code to solve instead of writing multiple tests first
* Starting to work on block comments, but still hasn't hit token/tracking yet
* gave a small poke about parsing line by line
    * Still sticking with line by line
    * But did hit on tracking inComment
* I gave him a big nod for string litterals
* Ah, switching to building this into an object to track state
* Taking advantage of reading through language builtins via Intellij
    * Came across StreamTokenizer - this is the one tool I said is not allowed
* Time ran out
    * Hit on needing to peek at next char to determine where we're at in the parser
    