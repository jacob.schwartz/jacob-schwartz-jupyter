# Mikhail Golubitsky - Sr SDE
## Java comment parsing
## Summary

## Checklist
*  Able to come up with optimal, simple algorithm. 

*  Code is efficient and performant

*  Able to come up with an extensive list of test cases. 

*  Able to proactively ask questions to clarify problem ambiguity. 

*  Able to produce clean, extensible code that implements the algorithm. 

*  Respond to interviewer questions and feedback, adjust their approach based on questions/feedback

*  Able to make trade-offs and justify decision making approach

*  Able to explain runtime complexity of the code produced

*  Code is easy to read and easy to maintain (modularized; with appropriate level of abstractions)

*  Code handles most edge cases.

## Gdoc

## Raw Notes: