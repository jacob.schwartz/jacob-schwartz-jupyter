# Name - Sr SDE - Sys Design

## Summary

## Leveling
* Able to ask clarifying questions to understand the system requirements
* Able to come up with reasonable detailed design for a sub-system (front end; or back end; or ingestion)
* Able to come up with reasonable data model 
* Demonstrate basic knowledge of cloud-based system
* Able to understand system bottlenecks 
* Logical reasoning; Open to different options; Aware of some assumptions made
* Able to take feedback and adjust approach 
* Able to communicate design ideas and why clearly 
* Aware of all key assumptions made
* Design is easy to understand, modular, easy to extend
* Able to ask clarifying questions to understand non-functional requirements (system performance, scale, stability SLA) 
* Able to identify scaling bottlenecks and design systems for increased scale
* Able to explore the extension problems

## Diagram Link


## Raw Notes
* Initial
* Data Injestion
* Database
* Front End
* Failure cases
* Bird cages
    