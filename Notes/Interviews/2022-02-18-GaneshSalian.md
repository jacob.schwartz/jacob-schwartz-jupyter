# Ganesh Sailan - Sr SDE - Sys Design

## Summary
Overall, I am leaning to hire Ganesh, depending on the other interviews. Ganesh had a fairly solid grasp on the back-end design, but had some major misses on the front-end interaction. Ganesh wasn't able to handle the ambiguity on the address/lat-lon as well as I'd like and when pressed about how the front end would limit the query space (providing some location bounds, etc) he wasn't able to provide a good solution. This may have been chalked up to nerves. However, his back-end design was solid and he showed that he has a good grasp of cloud systems. 

## Leveling
* Able to ask clarifying questions to understand the system requirements
    * Yes
* Able to come up with reasonable detailed design for a sub-system (front end; or back end; or ingestion)
    * Yes
* Able to come up with reasonable data model 
    * Somewhat - would like to see a more purposful design around location
* Demonstrate basic knowledge of cloud-based system
    * Yes
* Able to understand system bottlenecks 
    * Somewhat
* Logical reasoning; Open to different options; Aware of some assumptions made
    * Somewhat
* Able to take feedback and adjust approach
    * Yes
* Able to communicate design ideas and why clearly 
    * Mostly, missed on the front end
* Aware of all key assumptions made
    * Somewhat
* Design is easy to understand, modular, easy to extend
    * Yes
* Able to ask clarifying questions to understand non-functional requirements (system performance, scale, stability SLA) 
    * Somewhat
* Able to identify scaling bottlenecks and design systems for increased scale
    * Didn't get time
* Able to explore the extension problems
    * Didn't get time

## Diagram Link
https://whimsical.com/system-design-ganesh-salian-sr-sde-8j8J9VQX95Zsqo8XgMGxpG

## Raw Notes
* Initial
    * Showed he actually did research about flexe
    * Asked about data injestion before I got a chance to talk about it - good sign
    * 2 services
        * Aggregator (parsing)
        * API service
* Data Injestion
    * Query email/file server once a day
    * Validation
        * Use a master/list/enum of bird types
        * Otherwise general validation of data input
    * Don't pull files already procesed
        * Keep track of last run date
        * Only pull emails 
    * Pre processor
        * Convert address/zip entries into latitude/longitude
        
* Database
    * No SQL
        * Faster access
        * Seemingly unorganized data
        * Familiar with couchbase
        * 
* Front End/API service
    * Assumes heavy traffic
    * Components
        * Website
        * Load balancer
        * API Gateway
            * using old style endpoints /lat/{lat}/lon/{lon}
        * Caching mechanism
            * Refresh cache once per day or as often as the email job runs
            * key by user request
    * Ooof starting to flounder on how to filter/reduce data coming back
        * 
* Failure cases
    * Availability
        * Splunk? Does splunk track beyond logging?
    * latency
    * 
* Scaling
* Bird cages