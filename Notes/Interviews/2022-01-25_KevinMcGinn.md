# Kevin McGinn - Sr. SDE
## Meeting Scheduler

## Doc
https://drive.google.com/open?id=10CE0EQC8Q7TXGyMn7u3qfGLFrvv1jOePTVumS9XZgog&authuser=0


## Summary
Depending on the other interviews, I'm inclined to hire Kevin as a Sr. SDE. He showed all of the personality aspects of a good Sr SDE, planning before coding, asking questions and taking feedback. He went with the double pointer approach which is very difficult but arrived at what looks like a decent solution. 

However, he had not made sure to prep everything in his IDE before the interview. While he had experience with unit/automated testing in VisualStudio before, he had not set up a teste project on his own before. I sent him a quick article, but he did not get to making the unit test project until very close to the end of the interview, so he only completed one test case. This is a bit concerning to me, but if other interviews went well, it's something that may just be a one-off. 

## Checklist
* Able to come up with optimal, simple algorithms. 
    * Yes
* Code is efficient and performant
    * Yes
* Able to come up with an extensive list of test cases. 
    * Yes, but didn't finish implementing them
* Able to proactively ask questions to clarify problem ambiguity. 
    * Yes
* Able to produce clean, extensible code that implements the algorithm. 
    * Yes
* Respond to interviewer questions and feedback, adjust their approach based on questions/feedback
    * Yes
* Able to make trade-offs and justify decision making approach
    * Middling here 
* Able to explain runtime complexity of the code produced
    * Yes
* Code is easy to read and easy to maintain (modularized; with appropriate level of abstractions)
    * Yes
* Code handles most edge cases.
    * Somewhat, didn't get through unit tests
    
## Raw Notes:

### Intro
* C# is language chosen
* Intro
    * Questions up front
        * Timzeones? - No, don't worry about it
        * What does input look like? - You can design it, within reason
        * Multiple days? How big are the calendars? - Might be multiple days
        * Writing console app? - No, just worry about library
        * Clarified return data well
* Seems confident up front, I think Kevin understands it well
* Some odd misses in IDE knowledge, nothing big. Didn't know how to use some features of Visual Studio that normally C# devs know how to do


### Initial Impl
* Thinking through things pretty well
* Almost started coding without really going over test cases - had to be proded but immedialty started writing several good 
* Starts with method signature
    * Using "Meeting" class with start+end as DateTime
    * Wish he would have immediately gone to the list of cals instead of just taking 2 inputs
    * Considering using Set for uniqueness
    * Talking over iteration
    * 
* Test cases
    * Empty cals
    * Full cals
    * Basic human readable example
    * Overlaping within one cal
    * No possible common slot available
    * 
* Caught potential for a single user to have overlapping meetings (multiple meetings at the same time)


### Checkin 1 - 2:55pm
*  Good chunk of work done
* Looking to extract meetings starting or ending within target range
    * Surprising he didn't use LINQ here, much faster to write
* Going for finding open gaps and merging approach
* No unit tests yet - Ah because he forgot where the unit test creation is in VS
* Seems to have good understanding of problem and good idea for solution
* 


### Checkin 2 - 3:30pm
* Caught meeting edge edgecase
* 

### Final Checkin - 4:15pm
* His IDE gave problems so I gave some extra time, perhaps too much
    * Part of the issue was that he didn't make sure he knew how to create a unit test project before starting
* Did at least show some good debugging skills
    * Didn't want to give up
* Only got empty test case
* Explained his algorithm well
* Went with double cursor approach
