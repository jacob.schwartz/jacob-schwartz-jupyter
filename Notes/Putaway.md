# Putaway
* Articles
    * https://www.mmh.com/article/putaway_101_everything_in_its_place

* General/Industry info
    * Once goods have been received and staged, they need to be buffered
        * Pre-process
            * Conventional storage
            * Racks, shelves, floor for demand
            * May be moved to pick area, or truck
        * In-process
            * Pick wall, conveyor
            * Moving items directly into a process (like breaking down cases?) instead of storage
        * Post-process
            * Buffers specifically allocated for outbound recipients?
            * Outbound ship staging
    * Crossdock
        * Goods might move directly to outbound
        * Buffers may still be required
    * Pre-process/storage is the most used?
    * AS/RS - Automated storage / retrieval systems
        * Might include cranes, mini-load systems, carousels, multi-shuttles
    * Directed putaway
        * Operator scans barcode
        * WMS knows what is being put away
        * WMS recommends some zones
            * Closest aisle to its pick module
            * Full pallet area if it's never replenished
            * Based on usage
            * Restricted storage
        * WMS will track the item through the put away process
        * Mixed case (multi-sku?) palets need to be moved/put away in specific order
        * Lot organization (set of goods manufactured at the same time) is required
            * Expiration date, etc. 

## Flexe specific notes
* KT docs: https://drive.google.com/drive/u/0/folders/1zlUga-GXBQhdw1mNtgecxE71WokkXAH_
* Original design by Mingchen and others: https://docs.google.com/document/d/1eOh4cv3gbTati6Ax6RTCDhl2CELS-i8E3gbh8zsHGa4/edit?usp=sharing
* Docs: https://drive.google.com/drive/u/0/folders/1zlUga-GXBQhdw1mNtgecxE71WokkXAH_
