# Recieving

## General Information
<https://www.mmh.com/article/receiving_101_setting_the_table_for_success1>


* Remember, garbage in, garbage out.
* Receiving is the entry point for the warehouse
* If we get this right, things will go much more smothely
* If wrong, it becomes an exponential headache

### Preping for prodctivity
* ASNs increase efficiency through prep
* Need to also focus on collab with suppliers
* Try to match PO with shipment information - try to include on BOL
* Time, price and service level are all factors

### The knock at the door
* ASNs allow carriers to prepare the warehouse
* Mode of shipping and pallet vs loose


## Flexe