# Questions to find answers to

## Onboarding


## My product
* What is **Commingling**?
    * Special rules that define where items are allowed to share space
* What is **MoveThat**?
    * Application that helps clients move items around the warehouse. 
    * Items coming off the truck, but any physical movement of items
    * **Picking** and **Packing** sometimes go through other apps
* What is **LPN**?
    * License plate number -- used for pallets
* What is **UOM**?
    * Unit of measurement
    * Eaches or Cartons
* What are the "different receiving experiences"?
* Who are our clients? Internal, external or both?
* Where does the IQ platform sit in relation to the other components for the Flexe system?
* What is the difference between auditing and cycle counts?
* What is an "appointment"?
* What is "capacity" in a reservation?
* What is FRITO?
* Where is our serivce architecutre?
