

## Systems Design 
### Birds
* Ask about what happens when failures happen during ingestion
* Ask about tradeoffs for services
* Ask about scalability - what happens when we start getting 10x, 100x readers?
* How to deal with duplication of data
    * Multiple users report the same sightings?
* What would the KPI's be?
* More indepth feedback on Lever
* Keep flexe values in mind