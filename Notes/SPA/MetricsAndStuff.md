# Suggested Put-Away Metrics and Etc.


## Caveats
* Racks/Locations configurations will change
    * Somewhat drastic - probably at least quarterly
    

## Metrics for improving put away
* Target optimizing Ecommerce & Retail
    * Velocity out of locations for pick
    * Average number of locations required for a pick
    * Time between pick and pack
* Overall velocity of SKU
    * Slower skus should be moved to "colder" storage spaces
* 
* Rate at which suggested locations are used/ignored
* Reason selection for ignoring suggestion
    * Ask user for reason suggested location was not used for a sku
* Average put-away time
* Average pick time
* Pedometer?
* Accellerometer?
* Time spent per move
* More location attributes
    * Pallet storage
    * Case Flow (5-7 deep)
    * Bin Storage (eaches are dumped into cartons)



## Suggestion Strategies
* Consolodation
    * Current DPA approach
    * Group same SKUs together - sort by qty of sku decsending
    * Improve:
        1. Primary pick
        2. Largest Qty of SKU
        3. Last location with SKU
        4. Consolidate based on uom?
* Velocity
    * Find location that has most of given SKU moved OUT of given location
    * Specifically for Retail/Ecomm
* Zones
    * If we can get warehouses on board....
    * Zones per res, etc.
    * Configurations
        * Set sku locations here
        * Set default/override location type
* Random


## Associated Projects?
* Dashboard for warehouse metrics
    * Pick velocity/efficiency per SKU
        * Number of locations visited
    