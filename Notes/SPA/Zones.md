# Adding Zones
## Targeting SPA/mDesign first, but looking into the future

## 03/07/2022
* What are all the potential usecases?
    * Picking
        * From Shahid - https://flexe-inc.atlassian.net/wiki/spaces/TA/pages/2438168632/Lowe+s+Zone+Picking
    * Replen
    * mDesign - SuggestedPutAway
    
### mDesign 
* Categorize goods by velocity
    * Spec. velocity out of the warehouse
    * Out of scope - systematically calculating velocity
    * Velocity is an attribute of item's properties
        * Is it based on sku or UoM?
* Target goal:
    * Optimize outbounding
     * Note: Define what this means?
* **VELOCITY CHANGES THROUGH TIME**
    * Many of the goods stored at our warehouses are seasonal - velocity will be different based on time in year
* 


## 03/14/2022
* Meeting w Daltas, Lindsey, Chris V. 
* Multiple zones per location??
* 