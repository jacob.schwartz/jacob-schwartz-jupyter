# Where shit is in Warehouser

`rake:routes` is your friend. Use it to help you find controllers by their endpoints

## Rough guide
* Webapp: controllers/shipper and controllers/warehouser
* Mobile: services/mobile/controllers
* Api: all over the place

## Routing model
Browser -> Istio -> routes.rb -> controller

## Models
* Control SQL 
* 1 model gets 1 table and required join tables


## Shipper
* Dropoff
    * controllers/shipper/dropoffs/
        * container controller -> new/primary
        * palletized controller -> old/deprecated
    * views/shipper/dropoff
        * old haml erb 

## Warehouse
* Dropoff
    * controllers/warehouser/dropoffs/
        * container controller -> new/primary
        * palletized controller -> old/deprecated
    * views/warehouser/dropoffs
        * palletization -> container
            * Renders react components in app/javascript/components/dropoffs/Dropoffs.tsx
        * palletized
    