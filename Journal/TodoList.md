# My Todo List
* Read over the 30/60/90 document https://docs.google.com/document/d/1yU6O71WwtFtxmIoVVvw6Vgx3DpYDVCPr2Anrf7E9jzE/edit?ts=60c7a180
* Set up meetings with key department members
    * Seth Johnson - SDM of Transportation - Sent
* Set up meetings with Cross Functional Stakeholders
    * Logan Lambert - Business System Analyst
    * Sam Mueller - Senior Logistics Analyst
    * Chris Daltas - Lead UX Designer
    * Glenn McCrary - Sr Manager of Product
    * Jeff Werley - Principal Technical Program Manager
    * Questions to answer for each stakeholder
        * What does your role entail and what does your team own? 
        * How will our teams interact with each other?
        * How can I be most helpful to you?
        * How does your team ensure critical business processes are built for continued rapid scaling?
        * How does your team actively seek solutions and jump in?
            * What’s the best way to communicate with your team (email, slack)?
* Ask team about coffee breaks/happy hours?
* Lookup 'Site Reliability Engineering' Book and Workbook
* Look for new service deployment docs from #service-in-a-day

# My Done List
* IQ-301
* IQ-324
* IQ-351
* Finished setup script with Steven (6/16/21)
* Scheduled 1-1 team meetings (6/16/21)
* Ask Dennis about our values, how we apply them to the team/product
* Ask Dennis about the Metabase access
* Anthony Girolamo - VP Engineering - sent
* Set up meetings with team members
    * Tyler - sent
    * Mingchen - sent
    * Molly - sent
    * Lindsey Peters - Product Manager of Inbound - sent
    * Aaron Webb - SDM of IMS and OMS - Sent
    * Luke Weber - Principal SRE - sent
    * Jeka Pryhodko - SDM of SA (pack and outbound) - Sent
    * Rami Arowesty - SDM of GOLD (Slotting, Wave, Batch, Pick) - sent
    * Steven Stevenson - SDM of Integrations - sent
    * Rebecca Nelson - SDM of Billing - sent