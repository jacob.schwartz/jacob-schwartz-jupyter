# Flexe work jounal

## 08/30/2021
* Learning Scala

## 08/24/2021
* Still more antivirus - need to write HTTP wrapper because TCP sockets can't be load balanced

## 08/23/2021
* More work on Antivirus doc
* Searching for Reflected XSS

## 08/02-06/2021
* Mobile, mobile, mobile
* Trying to figure out how to create a pick-to-carton

## 07/23/2021
* Finished first mobile ticket
* Working on the CC mobile scrolling issue

## 07/20/2021
* Got a modal to work in 

## 07/19/2021
* Finally got mobile app running


## 07/14/2021
* Working n the frontend tests
* Talking about seeder

## 07/08/2021
* Working on IQ-325
* Finished react tic tac toe tutorial

## 07/07/2021
* Finished IQ-354
* Working on IQ-325
* Talked with Vansh
* 

## 07/01/2021
* Zendesk training
* Working on IQ-321
* Helping Molly with debugging

## 06/30/2021
* Zendesk training
* Working on IQ-321
* Helped Lauren with training

## 06/29/2021
* Zendesk university
* Merged? that bug
* Working on other bugs

## 06/28/2021
* Finally finished that bug, just waiting on MR
* 1-1 with Aaron
* 1-1 with Lindsey

## 06/23/2021
* Bug snag, UI/UX needed some work
* Interviewed Joe
* Helped dipen w setup
* Flexe mission meeting w Karl


## 06/22/2021
* More bug progress, session with Dipen
* 1-1 with Chris E. - interview mentor
* 1-1 with Anthony


## 06/21/2021
* Made some good headway on the bug
* 1-1 with Tyler
* 1-1 with Mangchen
* 1-1 with Molly

## 06/17/2021
* 1-1 with Dennis
* Ruby Koans (110/282)
* Learned info about the warehouser from Richard
* More reading on the space

## 06/16/2021
* Finished the setup script!
* Got to chat with Richard
* Started working on Ruby koanes
* Values training
* Got to give (hopefully) good feedback/suggestions in IQ tech overview


## 06/15/2021
* Completed I-9 paperwork
* Started work on the setup scripts
    * Ran into some issues with bash vs. zsh on macOS - adding changes for Steven
* Reading through 30/60/90
    * Finished team manifest
* Singed up for Benefits and got most of PTO in
* Met with Dipen, will work with him on his bug when environment is set up and have more info about product - target for next week


## 06/14/2021
* First day!
* Need to finish up I-9 paperwork
* Had meeting with Dennis